package ftc.shift.sample.models.enums;

public enum UserTypeEnum {
    ACTIVE,
    PASSIVE;

    UserTypeEnum() {

    }
}
