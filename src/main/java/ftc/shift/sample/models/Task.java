package ftc.shift.sample.models;

import ftc.shift.sample.models.enums.TaskStatusEnum;
import ftc.shift.sample.models.enums.TaskTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table
@Api(description = "Запросы для работы с задачами")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Уникальный идентификатор задания", required = true)
    private Long id;

    @ManyToOne
    @ApiModelProperty(value = "Исполнитель задания")
    private User executor;

    @ManyToOne
    @ApiModelProperty(value = "Постановщик задания", required = true)
    private User employer;

    @NotEmpty(message = "Provide address")
    @ApiModelProperty(value = "Адрес расположения задания", required = true)
    private String address;

    @NotEmpty(message = "Provide short description")
    @ApiModelProperty(value = "Краткое описание задания", required = true)
    private String descriptionShort;

    @NotEmpty(message = "Provide full description")
    @ApiModelProperty(value = "Полное описание задания", required = true)
    private String descriptionFull;

    @Enumerated(EnumType.ORDINAL)
    @ApiModelProperty(value = "Статус задания", allowableValues = "ACCEPT, PROCESS, DONE")
    private TaskStatusEnum status;

    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @ApiModelProperty(value = "Дата создания задания")
    private Date createdDateTime;

    @UpdateTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @ApiModelProperty(value = "Дата обновления задания")
    private Date updatedDateTime;

    @NotNull
    @ApiModelProperty(value = "Тип задачи", allowableValues = "PRIVATE, SOCIAL", required = true)
    private TaskTypeEnum type;

    @NotNull
    @ApiModelProperty(value = "Стоимость в Дукалисах(Для личной) или в Карме(Для общественной)", required = true)
    private Integer price;

    @ApiModelProperty(value = "Координаты задания", required = true)
    private String pointOnMap;

    public Task(){}

    public Task(User executor, User employer, String address,
                String descriptionShort, String descriptionFull,
                TaskStatusEnum status, Date createdDateTime,
                Date updatedDateTime, String pointOnMap) {
        this.executor = executor;
        this.employer = employer;
        this.address = address;
        this.descriptionShort = descriptionShort;
        this.descriptionFull = descriptionFull;
        this.status = status;
        this.createdDateTime = createdDateTime;
        this.updatedDateTime = updatedDateTime;
        this.pointOnMap = pointOnMap;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getExecutor() {
        return executor;
    }

    public void setExecutor(User executor) {
        this.executor = executor;
    }

    public User getEmployer() {
        return employer;
    }

    public void setEmployer(User employer) {
        this.employer = employer;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getDescriptionFull() {
        return descriptionFull;
    }

    public void setDescriptionFull(String descriptionFull) {
        this.descriptionFull = descriptionFull;
    }

    public TaskStatusEnum getStatus() {
        return status;
    }

    public void setStatus(TaskStatusEnum status) {
        this.status = status;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getPointOnMap() { return  pointOnMap; }

    public void setPointOnMap(String point) { this.pointOnMap = point; }

    public TaskTypeEnum getType() {
        return type;
    }

    public void setType(TaskTypeEnum type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
