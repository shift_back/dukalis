package ftc.shift.sample.utils.sorting;

import ftc.shift.sample.models.Task;

import java.util.List;
import java.util.stream.Collectors;

public class SortByDistance implements SortBy{
    private List<Task> tasks;
    private String strPoint;
    private PointMap point;
    private static final int EARTH_RADIUS = 6372795;

    public SortByDistance(List<Task> tasks, String strPoint) {
        this.tasks = tasks;
        this.strPoint = strPoint;
        this.point = PointMap.convertString(strPoint);
    }

    private float calculateDistance(PointMap point, PointMap taskPoint) {
        if (point == null || taskPoint == null) {
            return Float.POSITIVE_INFINITY;
        }

        double lat1 = point.getX() * Math.PI / 180.0;
        double lat2 = taskPoint.getX() * Math.PI / 180.0;
        double long1 = point.getY() * Math.PI / 180.0;
        double long2 = taskPoint.getY() * Math.PI / 180.0;

        double cl1 = Math.cos(lat1);
        double cl2 = Math.cos(lat2);
        double sl1 = Math.sin(lat1);
        double sl2 = Math.sin(lat2);
        double delta = long2 - long1;
        double cdelta = Math.cos(delta);
        double sdelta = Math.sin(delta);

        double y = Math.sqrt(Math.pow(cl2 * sdelta, 2) + Math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
        double x = sl1 * sl2 + cl1 * cl2 * cdelta;

        double ad = Math.atan2(y, x);

        return (float)ad * EARTH_RADIUS;
    }

    @Override
    public List<Task> sorting() {
        return tasks.stream()
                .sorted((Task task1, Task task2) ->
                        (int)(calculateDistance(point, PointMap.convertString(task1.getPointOnMap())) -
                                calculateDistance(point,PointMap.convertString(task2.getPointOnMap())))
                )
                .collect(Collectors.toList());
    }
}
