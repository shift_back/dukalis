package ftc.shift.sample.models.enums;

public enum TaskStatusEnum {
    ACTIVE,
    PROGRESS,
    DONE,
    DELETED;

    TaskStatusEnum() {

    }
}
