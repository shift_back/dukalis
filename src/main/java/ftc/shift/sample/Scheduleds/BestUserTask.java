package ftc.shift.sample.Scheduleds;

import ftc.shift.sample.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BestUserTask {

    private final UserService userService;

    @Autowired
    public BestUserTask(UserService userService) {
        this.userService = userService;
    }

    /**
     * Метод, который первого числа каждого месяца выбирает пользователя с наивысшей кармой
     */
    //@Scheduled(cron = "0 0 0 1 * *")
    // Для тестирования лучший пользователь выбирается 1 раз в минуту
    @Scheduled(cron = "0 * * * * *")
    public void testScheduled(){
        userService.setBestUser();
    }
}
