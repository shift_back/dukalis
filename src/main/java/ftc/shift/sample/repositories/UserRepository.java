package ftc.shift.sample.repositories;

import ftc.shift.sample.models.User;
import ftc.shift.sample.models.enums.UserRoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {
    User getUserByUsername(String username);

    // Запрос уменьшает значение кармы на 60%
    @Transactional
    @Modifying
    @Query(value = "UPDATE users SET karma = CAST((karma * 0.4) AS INTEGER)", nativeQuery = true)
    void resetKarma();

    // Запрос для получения текущих лучших пользователей
    @Query("SELECT u FROM User u WHERE u.roles = 2")
    List<User> getBestUsers();

    // Зарос для получения значения максимальной кармы
    @Query("SELECT max(u.karma) FROM User u")
    Integer getMaxKarma();

    // Получение всех пользователей с максимальной кармой
    List<User> findByKarma(Integer karma);

}
