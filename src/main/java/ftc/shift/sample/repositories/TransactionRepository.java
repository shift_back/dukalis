package ftc.shift.sample.repositories;

import ftc.shift.sample.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    // Получение Транзакций для пользователя по ID
    List<Transaction> findByUser_Id(Long userId);
}
