package ftc.shift.sample.services;

import ftc.shift.sample.exception.*;
import ftc.shift.sample.exception.PermissionDeniedException;
import ftc.shift.sample.exception.TaskNotFoundException;
import ftc.shift.sample.models.Task;
import ftc.shift.sample.models.Transaction;
import ftc.shift.sample.models.User;
import ftc.shift.sample.models.enums.TaskStatusEnum;
import ftc.shift.sample.models.enums.TaskTypeEnum;
import ftc.shift.sample.models.enums.TransactionTypeEnum;
import ftc.shift.sample.repositories.TaskRepository;
import ftc.shift.sample.utils.sorting.PointMap;
import ftc.shift.sample.utils.sorting.SortByDistance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TaskService {

    private final TaskRepository taskRepo;
    private final UserService userService;
    private final TransactionService transactionService;

    public TaskService(TaskRepository taskRepo, UserService userService, TransactionService transactionService) {
        this.taskRepo = taskRepo;
        this.userService = userService;
        this.transactionService = transactionService;
    }

    /**
     * Обёртка для функции получения задачи по id
     * Генерирует исключение, если задачи не существует
     *
     * @param taskId Идентификатор задачи
     * @return Объект задача
     */
    public Task getTaskById(Long taskId) {
        return taskRepo.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId));
    }

    public Task getTask(Long userId, Long taskId) {

        return getTaskById(taskId);

    }

    /**
     * Метод для создания задачи
     * Проверяет, что дукалисов хватает, если задача Личная
     *
     * @param task   Задача, отправленная пользователем
     * @param userId Идентификатор пользователя
     * @return Созданная задача
     */
    public Task createTask(Task task, Long userId) {

        User user = userService.getUserById(userId);

        // Для создания задачи у пользователя должно быть больше -100 кармы
        if (user.getKarma() < -100) {
            throw new PermissionDeniedException("Must be at least -100 karma");
        }

        // Получаем тип задачи
        TaskTypeEnum typeTask = task.getType();

        // Если задача личная, то проверяем, что у
        // пользователя хватает дукалисов для ее создания
        // и корректную цену
        boolean isAddTransaction = false;
        Integer price = 0;

        if (typeTask.equals(TaskTypeEnum.PRIVATE)) {
            // Если у заказчика, есть бесплатные личные просьбы, используем их. Цена задания становится 500
            if (user.getFreePersonalTasks() > 0) {
                user.decrementFreePersonalTasks();
                task.setPrice(500);
                userService.updateUser(user);
            } else {
                Integer dukalisesUser = user.getDukalises();
                price = task.getPrice();
                if (price == null) {
                    throw new BadRequestException("Provide a price");
                }
                // Стоймость задачи не может быть меньше 1
                if (price < 1) {
                    throw new BadRequestException("The price cannot be less than 1");
                }
                if (price > dukalisesUser) {
                    throw new PermissionDeniedException("You have not enough dukalises:" + price);
                }
                isAddTransaction = true;
                // Забираем у пользователя дукалисы для выставления задачи
                user.setDukalises(dukalisesUser - price);
                userService.updateUser(user);


            }

        } else if (typeTask.equals(TaskTypeEnum.SOCIAL)) {
            // Для общественной задачи

            task.setType(TaskTypeEnum.SOCIAL);
            task.setPrice(1000); // Устанавливаем фиксированную стоймость (1000 кармы)
        } else {
            // Выдаем исключение о неизвестном типе задач
            throw new BadRequestException("Incorrect type of a task: " + typeTask);
        }

        task.setEmployer(user);
        task.setStatus(TaskStatusEnum.ACTIVE);
        task = taskRepo.save(task);
        if (isAddTransaction) {
            // Добавляем транзакцию
            Transaction transaction = new Transaction(
                    TransactionTypeEnum.PAYMENT,
                    null,
                    -price,
                    "Платёж за подачу заявки на помощь #" + task.getId(),
                    user
            );
            // Сохраняем транзакцию в БД
            transactionService.createTransaction(transaction);
        }
        return task;
    }


    public Task completeTask(Long taskId, Long userId) {
        userService.getUserById(userId); // Проверяем, что пользователь существует

        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId));

        // Если задачу пытается подтвердить не заказчик, формируем исключение
        if (!task.getEmployer().getId().equals(userId)) {
            throw new PermissionDeniedException("You are not a employer of this task");
        }

        // Если задача активна или завершена, её нельзя завершить
        if (task.getStatus().equals(TaskStatusEnum.ACTIVE)
                || task.getStatus().equals(TaskStatusEnum.DONE)) {
            throw new PermissionDeniedException("It is impossible to finish this task");
        }


        // Меняем статус задачи на 'Выполнена'
        task.setStatus(TaskStatusEnum.DONE);

        // Получаем исполнителя задачи
        User user = task.getExecutor();

        // Если задача личная, то начисляем исполнителю дукалисы и карму
        if (task.getType().equals(TaskTypeEnum.PRIVATE)) {
            Integer price = task.getPrice();
            int karma = price / 2; // 1 дукалис равен 0.5 кармы
            // Исполнитель не может получить больше 500 кармы
            if (karma > 500) karma = 500;

            user.addDukalises(price);
            user.addKarma(karma);

            // Добавляем транзакцию
            Transaction transaction = new Transaction(
                    TransactionTypeEnum.CHARGE,
                    null,
                    price,
                    "Начисление за выполнение заявки на помощь #" + task.getId(),
                    user
            );
            transactionService.createTransaction(transaction);

        } else {
            // В общественных задачах пользователь получает только карму
            Integer karma = task.getPrice();
            user.addKarma(karma);
        }
        // Сохраняем пользователя и задачу
        userService.updateUser(user);
        return taskRepo.save(task);
    }

    public Task applyTask(Long taskId, Long userId) {
        User user = userService.getUserById(userId); // Проверяем, что пользователь существует

        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId));


        // Заказчик не может принять свою задачу
        if (task.getEmployer().getId().equals(userId)) {
            throw new PermissionDeniedException("You cannot accept a task which you created");
        }

        // Нельзя принять задачу, если она не активна
        if (!task.getStatus().equals(TaskStatusEnum.ACTIVE)) {
            throw new PermissionDeniedException("The task is not available to acceptance");
        }


        task.setStatus(TaskStatusEnum.PROGRESS);
        task.setExecutor(user);
        taskRepo.save(task);
        return task;
    }

    /**
     * Реализует отмену задачи исполнителем
     *
     * @param taskId ID задачи
     * @param userId ID пользователя
     * @return Измененная задача
     */
    public Task cancelTask(Long taskId, Long userId) {
        User user = userService.getUserById(userId); // Проверяем, что пользователь существует

        Task task = taskRepo.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId));

        // Если пользователь не является исполнителем задачи, генерируем исключение
        if (task.getExecutor() == null || !task.getExecutor().getId().equals(userId)) {
            throw new PermissionDeniedException("You are not a executor of this task");
        }

        // Получем количество часов с момента принятия задания исполнителем
        long milliseconds = new Date().getTime() - task.getUpdatedDateTime().getTime();
        int hours = (int) (milliseconds / (60 * 60 * 1000));

        // Если прошло больше 24 часов, начисляем штраф
        // По 1 карме за каждый последующий час
        if (hours > 24) {
            int fine = hours - 24;
            user.addKarma(-fine); // Отнимаем карму у пользователя
            userService.updateUser(user);
        }


        task.setStatus(TaskStatusEnum.ACTIVE);
        task.setExecutor(null);
        taskRepo.save(task);
        return task;
    }

    /**
     * Получение списка задач по фильтрам
     *
     * @param status Статус задачи
     * @param userId Идентификатор пользователя
     * @param type   Тип задачи
     * @param page   Номер страницы
     * @param count  Количество элементов на странице
     * @return Список задач
     */
    public List<Task> getTasks(TaskStatusEnum status, Long userId, String type, TaskTypeEnum category, String peopleCoordinate,
                               int page, int count) {

        // Выбросит исключение, если пользователя не существует
        userService.getUserById(userId);
        PageRequest pageRequest = PageRequest.of(page, count);

        if (type.equals("PERSONAL")) {
            // Если статус не передан, то возвращаем все задачи, выставленные пользователем
            if (status == null) {
                return taskRepo.findByEmployer_Id(userId, new Sort("status"));
            } else {
                // Возвращаем задачи с указанным статусом, выставленные пользователем c
                return taskRepo.getPersonalTasksByStatus(userId, status, pageRequest).getContent();
            }
        } else {
            // Если статус не задан, то ACTIVE по умолчанию
            if (status == null) {
                status = TaskStatusEnum.ACTIVE;
            }
            // Если нужны доступные задачи, то возвращаем все задачи со статусом ACTIVE,
            // где пользователь не является заказчиком
            // в другом случае возвращаем задачи с нужным статусом, где исполнителем являемся мы
            // FIXME: Разобраться с кашей.
            if (status.equals(TaskStatusEnum.ACTIVE)) {
                // Если указаны координаты пользователя, то выдаём отсортированный список задач по расстоянию от пользователя
                if (peopleCoordinate != null) {
                    List<Task> tasksAll = taskRepo.findByEmployer_IdNot(userId);
                    SortByDistance sortedTasks = new SortByDistance(tasksAll, peopleCoordinate);
                    return sortedTasks.sorting();
                    // Если указана категория, то возвращаем доступные задания в категории
                } else if (category != null) {
                    return taskRepo.getPublicAvailableTasksByCategory(userId, category, pageRequest).getContent();
                    // Если никаких дополнительных параметров не переданы, то возвращаем все доступные
                } else {
                    return taskRepo.getPublicAvailableTasks(userId, pageRequest).getContent();
                }

            } else {
                // Возвращаем доступные задачи определенной категории
                return taskRepo.getPublicTasksByStatus(userId, status, pageRequest).getContent();
            }
        }

    }

    /**
     * Метод для жалобы на исполнителя
     *
     * @param userId Идентификатор пользователя
     * @param taskId Идентификатор задачи
     * @return Задача
     */
    public Task complainTask(Long userId, Long taskId) {
        // Получаем пользователя (Проверяем, что он существует)
        User user = userService.getUserById(userId);

        // Получаем задачу (Проверяем, что она существует)
        Task task = getTaskById(taskId);

        // Если жалобу подаёт не заказчик задачи, генерируем исключение
        if (!task.getEmployer().getId().equals(userId)) {
            throw new PermissionDeniedException("You are not a employer");
        }
        // Если задача не находится в статусе PROGRESS, генерируем исключение
        if (!task.getStatus().equals(TaskStatusEnum.PROGRESS)) {
            throw new PermissionDeniedException("The task is not in progress");
        }

        // Получаем исполнителя
        User executor = task.getExecutor();

        // Забираем у исполнителя 50% от стоймости заказа в карме, но не более 250
        Integer price = task.getPrice();

        // Для личных задач переводим дукалисы в карму
        if (task.getType().equals(TaskTypeEnum.PRIVATE)) {
            price = price / 2;
        }
        if (price > 250) price = 250;

        executor.addKarma(-price); // Отнимаем карму у исполнителя
        userService.updateUser(executor);

        task.setStatus(TaskStatusEnum.DONE);
        return taskRepo.save(task);
    }
}
