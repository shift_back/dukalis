package ftc.shift.sample.Scheduleds;

import ftc.shift.sample.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ResetKarmaTask {

    private final UserService service;

    @Autowired
    public ResetKarmaTask(UserService service) {
        this.service = service;
    }

    /**
     * Метод для уменьшения кармы пользователей 1 раз в три месяца
     */
    @Scheduled(cron = "0 0 0 1 */3 *")
    //@Scheduled(cron = "0 * * * * *")
    public void resetKarma(){
        service.resetKarmaForAllUsers();
    }

}
