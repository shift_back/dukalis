package ftc.shift.sample.services;

import ftc.shift.sample.models.Transaction;
import ftc.shift.sample.repositories.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {

    private TransactionRepository transactionRepo;
    private UserService userService;

    public TransactionService(TransactionRepository transactionRepo, UserService userService) {
        this.transactionRepo = transactionRepo;
        this.userService = userService;
    }

    /**
     * Метод для получение всех транзакций пользователя
     *
     * @param userId Идентификатор пользователя
     * @return Список транзакций
     */
    public List<Transaction> getTransactionsForUser(Long userId) {
        // Проверяем, что пользователь существует
        userService.getUserById(userId);

        return transactionRepo.findByUser_Id(userId);
    }

    /**
     * Метод для сохранение транзакции в БД
     *
     * @param transaction Транзакция
     * @return Информация о сохраненной транзакции
     */
    public Transaction createTransaction(Transaction transaction) {
        return transactionRepo.save(transaction);
    }
}
