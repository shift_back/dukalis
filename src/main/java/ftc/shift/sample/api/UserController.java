package ftc.shift.sample.api;

import ftc.shift.sample.models.User;
import ftc.shift.sample.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/v001/users")
@Api(tags = {"Запросы для работы с пользователями"})
public class UserController {

    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }


    @GetMapping
    @ApiOperation(value = "Получение списка всех пользователей")
    public List<User> listUsers() {
        return service.getAllUsers();
    }

    @PostMapping
    @ApiOperation(value = "Создание нового пользователя")
    public ResponseEntity<User> createUser(@Valid @RequestBody final User user) {
        return ResponseEntity.ok(service.createUser(user));
    }

    @GetMapping("/{userId}")
    @ApiOperation(value = "Получение пользователя по идентификатору")
    public User getUser(@PathVariable Long userId) {
        return service.getUserById(userId);
    }

    @GetMapping("/auth/{username}")
    @ApiOperation(value = "Авторизация пользователя по логину")
    public User authUser(
            @ApiParam(value = "Логин пользователя")
            @PathVariable String username) {
        return service.authUser(username);
    }


}
