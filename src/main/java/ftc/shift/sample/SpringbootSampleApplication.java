package ftc.shift.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * (Без необходимости не редактировать)
 */
@SpringBootApplication
@EnableScheduling
public class SpringbootSampleApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootSampleApplication.class, args);
  }
}
