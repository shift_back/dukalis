#!/usr/bin/env bash

./gradlew build

scp ./build/libs/backend-0.0.1.jar root@94.103.91.121:/projects/dukalis

ssh root@94.103.91.121 "bash -s" << EOF
    pkill java;
    cd /projects/dukalis/
    nohup java -jar backend-0.0.1.jar > log.txt &
EOF